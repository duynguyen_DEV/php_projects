<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin Page</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="header">
			<h3>Quan tri noi dung</h3>
		</div>

		<div class="menu">
			<ul>
				<li><a href="index.php">Trang chu</a></li>
				<li><a href="index.php?quanly=qlloaisp&ac=them">Quan ly loai SP</a></li>
				<li><a href="">QL chi tiet SP</a></li>
			</ul>
		</div>

		<div class="content">
				<?php
					require "modules/config.php";

					if(isset($_GET['quanly'])) {
						$tam = $_GET['quanly'];
					}
					else {
						$tam = "";
					}

					if ($tam == "qlloaisp") {
						require "modules/main.php";
					}
					
				?>
			
		</div>

		<div class="clear"></div>

		<div class="footer">
			
		</div>
	</div>
</body>
</html>