<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Website ban hang cong nghe</title>
	<link rel="stylesheet" href="css/styles.css">
</head>
<body>
	<div class="wrapper">
		<?php
			require_once "modules/header.php";
			require_once "modules/menu.php";
			require_once "modules/content.php";
		?>
		<div class="clear"></div>

		<?php	
			require_once "modules/footer.php";
		?>
	</div>
</body>
</html>