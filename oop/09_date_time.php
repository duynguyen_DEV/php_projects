<?php
    //ham date(format, timestamp)
    echo date('Y-m-d H:i:s');
    echo '<br/>';
    echo date('d/m/Y H:i:s');

    //su dung ham strtotime($bien_str) de doi sang format dac biet
    echo '<br/>';
    echo 'Hen ban 5 ngay sau tuc la ngay ' . date('d.m.Y', strtotime('+5 days')) . ' nhe!'; //cong them 5 ngay 
    echo '<br/>';
    echo 'Hen nuoc Italia 1 nam sau tuc la ngay ' . date('d.m.Y', strtotime('+1 year')) . ' nhe!'; //cong them 5 ngay 
    
    //hoac chung ta cung co the tru de hien thi nhung ngay, thang truoc do
    echo '<br/>';
    echo 'Da den Lourdres vao 2 thang truoc tuc la ngay ' . date('d.m.Y', strtotime('-2 months')) . ' roi!'; 

    //set mui gio cho he thong, nen tim hieu http://php.net/manual/fr/timezones.php
    echo "<h3>Mui gio hien hanh la " .date_default_timezone_get() ."</h3>";
    date_default_timezone_set("Asia/Ho_Chi_Minh");
    echo "<br>Sau khi set sang mui gio Sai Gon <br>";
    echo "<h1>Gio cua Saigon xinh dep la ". date("d/m/Y H:i:s") . "</h1>";
?>