<?php
    /* Cu phap 
        function ten_ham()
        {
            noi_dung ham
        }
    */

    function cong_2so_chan ($a, $b) {
        if ($a % 2 == 0 && $b %2 ==0) {
            $sum = $a + $b;
        } elseif ($a % 2 == 0){
            $sum = $a;
        } else {
            $sum = $b;
        }

        return $sum;
    }

    $num1 = 6;
    $num2 = 12;

    echo "So thu 1 co gia tri la $num1 va so thu 2 la $num2";
    echo '<br/>';
    echo "<h1>Tong chan la " . cong_2so_chan($num1, $num2) . "</h1>";
?>