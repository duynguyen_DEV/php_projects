<?php
    /* Cookie va Session dung de luu tru thong tin nguoi dung 
        -   Cookie thi luu tren may nguoi dung
        -   Dung ham setcookie()
    */
    /*
    $cookie_name = 'user';
    $cookie_value = 'Johnny';
    setcookie($cookie_name, $cookie_value, time() + 300); //luu tru cookie trong 300 giay
    echo $_COOKIE[$cookie_name];

    //de huy cookie ta setcookie nhu ben duoi
    setcookie($cookie_name, "", time() - 3600);
    echo $_COOKIE[$cookie_name];
    */

    /* Session 
        - duoc luu tru tren server
    */

    session_start();
    $_SESSION['username'] = 'admin';
    echo $_SESSION['username'];

    // de huy session ta dung 
    session_unset();
    session_destroy();
    print_r($_SESSION);
?>