<?php
    $a = 5;

    echo "Gia tri ban day cua bien a la $a <br>";

    $a +=1;

    echo "Toan tu a += 1 tuc la tang a len 1 don vi $a <br>" ;

    $a -= 1;

    echo "Toan tu a -= 1 tuc la giam a xuong 1 don vi $a <br>" ;

    $a *= 2;

    echo "Toan tu a *= 2 tuc la nhan a cho 2 = $a <br>" ;

    $a /= 2;

    echo "Toan tu a /=  tuc la chia a cho 2 = $a <br>" ;

    $a %= 2;

    echo "Toan tu a %= 2 tuc la lấy phần dư = $a <br>" ;

    echo "<h2>Tiếp theo là các toán tử so sánh</h2>";

    $a = 5;
    $b = "5";

    echo "Toán tử so sánh bằng với ==<br>";
    echo "<br>ta có a = $a và b = $b<br>";
    if($a == $b) {
        echo "Đúng";
    } else{
        echo "Sai";
    }

    echo "Toán tử so sánh bằng với ===<br>";
    echo "<br>ta có a = $a và b = $b<br>";
    if($a === $b) {
        echo "Đúng";
    } else{
        echo "Sai";
    }

    echo "<br>Kết luận: với toán tử ===, phép so sánh chỉ <strong>Đúng</strong> khi cả giá trị và kiểu dữ liệu đều giống nhau<br>";

