<?php
    /* 
        Xem them tai https://www.tutorialspoint.com/php/php_object_oriented.htm
        demo ve class */

    class Book 
    {
        //day la thuoc tinh - properties
        var $title;
        var $price;
        var $publishYear;
        var $authorName;

        //day la phuong thuc - method
        function setPrice($p) 
        {
            $this->price = $p;
        }

        function getPrice() 
        {
            echo $this->price;
        }
    }

    //khoi tao 1 object tro den doi tuong Book
    $book1 = new Book();
    $book1->setPrice('99,99');
    $book1->getPrice();
