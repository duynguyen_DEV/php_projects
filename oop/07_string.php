<?php
    $str = "Bonjour tout le monde";

    echo "<br>Hàm <strong>strlen()</strong> trả về độ dài của chuỗi có tên là str với nội dung là $str và độ dài là " . strlen($str) ." ký tự <br>";

    echo "<br>Hàm <strong>str_word_count</strong> để đếm số từ trong chuỗi : " . str_word_count($str) . " <br>";

    echo strrev($str) ." Oh,gì thế? Chuỗi bị đảo rồi hả? Bằng hàm <strong>strrev(chuỗi)</strong> <br>";

    echo "<br>Vị trí chuỗi <strong>tout</strong> trong chuỗi gốc với hàm <strong>strpos(chuỗi gốc, chuỗi con)</strong> là " . strpos($str, "tout") . "<br>";

    echo "<br>Thay thế chuỗi Bonjour bằng Bienvenue với hàm str_replace(search, replace, subject) => " . str_replace("Bonjour", "Bienvenue",$str);