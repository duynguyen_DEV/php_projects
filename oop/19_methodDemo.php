<?php
    /* 
        Xem them tai https://www.tutorialspoint.com/php/php_object_oriented.htm
        demo ve class */

    class Book 
    {
        //day la thuoc tinh - properties
        var $title;
        var $price;
        var $publishYear;
        var $authorName;

        //phuong thuc khoi tao constrcutor voi cu phap function __construct()
        function __construct() 
        {
            echo "class book will visit in this constructor.";
        }


        //day la phuong thuc - method
        function setPrice($p) 
        {
            $this->price = $p;
        }

        function getPrice() 
        {
            echo $this->price;
        }
    }

    $book1 = new Book();
