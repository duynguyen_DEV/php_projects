<?php
    //Dung try catch de bat loi
    try {
        throw new Exception("<h1>Loi fatal error</h1>");
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }

    //chung ta cung co the custom Exception
    function customException ($exception) {
        echo "<b>Exception error: </b>" . $exception->getMessage();
    }

    set_exception_handler("customException");

    throw new Exception("Loi fatal error");