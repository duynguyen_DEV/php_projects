<?php
    $connect = mysqli_connect("localhost","root", "", "banhangcongnghe");
    if($connect) {
        echo "Connected to DB Server";
    } else {
        die ("Failed");
    }

    echo "<br>";

    
    /*     Liet ke tat ca ca loai san pham trong bang loai SP */
    /*
    echo "Liet ke toan bo du lieu trong bang Loai San Pham <br>";
    $sqlStr = "SELECT * FROM loaisp";
    $result = $connect->query($sqlStr);
    while ($row = $result->fetch_assoc()) {
        echo "ID : " .$row['id_loaisp'] . " - Category Name : " . $row['tenloaisp'] . "<br>";
    }
    */

    /* Liet ke co dieu kien, vi du theo ID */
    echo "<br> Loc theo ID = 2 <br>";
    $sqlStr = "SELECT * FROM loaisp WHERE id_loaisp=2";
    $result = $connect->query($sqlStr);
    while ($row = $result->fetch_assoc()) {
        echo "ID : " .$row['id_loaisp'] . " - Category Name : " . $row['tenloaisp'] . "<br>";
    }

    /* Tan cong dang SQL INJECTION */
    echo "<br> Bi Tan Cong SQL Injection<br>";
    $id = "'' or 1=1";
    $sqlStr = "SELECT * FROM loaisp WHERE id_loaisp=$id";
    $result = $connect->query($sqlStr);
    while ($row = $result->fetch_assoc()) {
        echo "ID : " .$row['id_loaisp'] . " - Category Name : " . $row['tenloaisp'] . "<br>";
    }

    /* De tranh bi tan cong ta lam theo cach ben duoi */
    echo "<br> Ta dung cach sau de giam thieu kha nang bi tan cong<br>";
    $id = 1;
    $sqlStr = "SELECT * FROM loaisp WHERE id_loaisp=?";
    
    $stmt = $connect->prepare($sqlStr);
    $stmt->bind_param('i', $id);
    $stmt->execute();

    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        echo "ID : " .$row['id_loaisp'] . " - Category Name : " . $row['tenloaisp'] . "<br>";
    }