<?php
    include('db_connect.php');

    if(isset($_POST['login'])) {
        // build a function to validate data
        function validateFormData ($formData) {
            $formData = trim (stripslashes(htmlspecialchars($formData)));

            return $formData;
        }

        // create variables
        // wrap the data with our function
        $formUser = validateFormData($_POST['username']);
        $formPwd = validateFormData($_POST['password']);

        // create SQL query
        $query = "SELECT username, email, password FROM users WHERE username='$formUser'";

        // store the result
        $result = mysqli_query($conn, $query);

        // verify if result is returned 
        if (mysqli_num_rows($result) > 0) {
            //store basic user data in variables
            while ($row = mysqli_fetch_assoc($result)) {
                $user = $row['username'];
                $email = $row['email'];
                $hashedPwd = $row['password'];
            }
            
            // verify hashed password with the typed password
            if (password_verify($formPwd, $hashedPwd)) {
                //correct login details
                //start the session
                session_start();

                //store data in SESSION variables
                $_SESSION['loggedUser'] = $user;
                $_SESSION['loggedInEmail'] = $email;

                header("Location: profile.php");
            } else { // hashed password didn't verify
                //error message
                $loginError = "<div class='alert alert-danger'>Wrong username / password. Please try again</div>";
            }
        } else { // there's no result in database
            //error message
            $loginError = "<div class='alert alert-danger'>User doesn't exist. Please try again
                            <a class='close' data-dismiss='alert'>&times;</a></div>";
        }

        //close the connection
        mysqli_close($conn);
    }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style>
        input {
            margin-right: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1 class="text-center text-info">Login System</h1>
        <p class="lead">Use this form to log in to your account</p>

        <?php echo $loginError; ?>

        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form-inline">
            <div class="form-group">
                <label for="login-username" class="sr-only">Username</label>
                <input type="text" class="form-control" name="username" id="login-username" placeholder="Your username">
            </div>

            <div class="form-group">
                <label for="login-password" class="sr-only">Password</label>
                <input type="password" class="form-control" name="password" id="login-password"  placeholder="Your password">
            </div>
            <button class="btn btn-default" type="submit" name="login">Login</button>
        </form>
    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>