<?php
    // did the user's browser send a cookie for the session?
    if (isset($_COOKIE[session_name()])) {
        // empty the cookie
        setcookie(session_name(), '', time() - 86400, '/');

        //clear all the session variables
        session_unset();

        //destroy the session
        session_destroy();

        $msg = "You've been logged out! See you next time";
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Logout</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
   
</head>
<body>
    <h2 class="text-info"><?php echo $msg; ?></h2>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>